/**
* This is a supplementary material for the paper:
*   On Two LZ78-style Grammars: Compression Bounds and Compressed-Space Computation
*   Symposium on String Processing and Information Retrieval (SPIRE), 2017.
* Copyright (C) 2017
*   Tomasz Kociumaka <kociumaka (at) mimuw.edu.pl> (corresponding author),
*   Dmitry Kosolobov <dkosolobov (at) mail.ru> (corresponding author),
*   Golnaz Badkobeh, Travis Gagie, Shunsuke Inenaga, and Simon J. Puglisi.
*
* For the description, read the readme file.
* --------------------------------------------------------------
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
* OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
* HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
* OTHER DEALINGS IN THE SOFTWARE.
**/
#include <map>
#include <algorithm>
#include <string>
#include "reduction.h"



std::pair<istring_t, std::vector<istring_t>> lzd_reduction(size_t sigma) {
    istring_t t;
    std::vector<istring_t> al{istring_t(1, 0), istring_t(1, 1)};

    while(true) {
        std::vector<istring_t> nxt_al;
        for (auto const &x : al) for (auto const &y : al) if (x <= y) {
            t += x+y;
                    nxt_al.emplace_back(x+y);
            if (nxt_al.size() == sigma) return {t, nxt_al};
        }
        al.swap(nxt_al);
    }
}

std::pair<istring_t, std::vector<istring_t>> lzmw_reduction(size_t sigma) {
    istring_t t;
    std::vector<istring_t> bl{istring_t(1, 0), istring_t(1, 1)};
    
    while(true) {
        std::vector<istring_t> nxt_bl;
        istring_t previous_phrase;
        for (auto const &x : bl) {
            for (auto const &y : bl) if (y < x) {
                t += y + x;
                nxt_bl.emplace_back(previous_phrase + y);
                nxt_bl.emplace_back(y + x);
                previous_phrase = x;
            }
            t += x;
            if (!previous_phrase.empty()) nxt_bl.emplace_back(previous_phrase + x);
            previous_phrase = x;

            if (nxt_bl.size() >= sigma) {
                std::sort(nxt_bl.begin(), nxt_bl.end());
                return {t, nxt_bl};
            }
        }
        bl.swap(nxt_bl);
    }
}

ctrl_string reduce(reduction_t *reduction, ctrl_string const &in) {
    std::map<uint32_t, size_t> M;
    for (auto c : in.str) M[c] = -1;
    auto r = reduction(M.size());
    size_t l = r.second[0].length();
    size_t tlen = r.first.length();
    size_t alph = 0;
    ctrl_string out;
    out.str = r.first;
    for (auto c : in.str) {
        if (M[c] == -1) {
            M[c] = alph;
            ++alph;
        }
        out.str += r.second[M[c]];
    }
    for (auto p : in.ctrls) {
        out.ctrls[tlen + p.first*l] = p.second*l;
    }
	out.print = [](uint32_t c) { return std::to_string(c); };
    return out;
}
