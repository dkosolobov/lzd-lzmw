/**
* This is a supplementary material for the paper:
*   On Two LZ78-style Grammars: Compression Bounds and Compressed-Space Computation
*   Symposium on String Processing and Information Retrieval (SPIRE), 2017.
* Copyright (C) 2017
*   Tomasz Kociumaka <kociumaka (at) mimuw.edu.pl> (corresponding author),
*   Dmitry Kosolobov <dkosolobov (at) mail.ru> (corresponding author),
*   Golnaz Badkobeh, Travis Gagie, Shunsuke Inenaga, and Simon J. Puglisi.
*
* For the description, read the readme file.
* --------------------------------------------------------------
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
* OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
* HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
* OTHER DEALINGS IN THE SOFTWARE.
**/

#include <vector>
#include <functional>
#include <stdint.h>
#include <cmath>
#include <map>

#include "generate.h"

static uint32_t k;

static istring_t Rep(int i, int j, std::function<istring_t(int)> f)
{
    if (i > j) return istring_t();
    istring_t res = f(i);
    while (i < j) res += f(++i);
    return res;
}
static istring_t Rep(int i, std::function<istring_t()> f)
{
    if (i <= 0) return istring_t();
    istring_t res = f();
    while (--i > 0) res += f();
    return res;
}
static istring_t a() { return istring_t(1,0); }
static istring_t b() { return istring_t(1,1); }
static istring_t c() { return istring_t(1,2); }
static istring_t d() { return istring_t(1,3); }
static istring_t D(int i) { return Rep(i, a) + b() + b() + Rep(k - i, a); }
static istring_t G(int i) { return b()+Rep(2 * i + 1, a)+b()+c()+Rep(1, i, [](int j) { return b() + Rep(j, a); }); }
static istring_t x() { return Rep(1, k / 2 - 1, [](int i) { return D(k) + D(k - i); }) + D(k) + Rep(k - 1, a); }


ctrl_string lzmw_large_generate(uint32_t _k) {
    k = _k;
    auto s = Rep(0, k - 1, G) + Rep(0, k, [](int i) { return D(i) + d();})
        + Rep(1, (int)std::log2(k), [](int i) { return c() + Rep((1 << i) + (1 << (i - 1)) - 1, a); })
        + d() + c() + Rep(k/2, x);
    ctrl_string out;
    out.str = s;
	out.print = [](uint32_t c) { 
		if (c <= 3) return std::string(1, 'a' + c); 
		else return std::string("#");
	};
    return out;
}

