/**
* This is a supplementary material for the paper:
*   On Two LZ78-style Grammars: Compression Bounds and Compressed-Space Computation
*   Symposium on String Processing and Information Retrieval (SPIRE), 2017.
* Copyright (C) 2017
*   Tomasz Kociumaka <kociumaka (at) mimuw.edu.pl> (corresponding author),
*   Dmitry Kosolobov <dkosolobov (at) mail.ru> (corresponding author),
*   Golnaz Badkobeh, Travis Gagie, Shunsuke Inenaga, and Simon J. Puglisi.
*
* For the description, read the readme file.
* --------------------------------------------------------------
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
* OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
* HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
* OTHER DEALINGS IN THE SOFTWARE.
**/
#include <vector>
#include "parse.h"
#include "substring_trie.h"


std::vector<phrase> lzd_parse(istring_t const &str) {
    substring_trie t(str);
    std::vector<phrase> parsing;
    for (size_t len, i = 0; i < str.size(); i += len) {
        size_t chars_read = 0;
        len = t.find_max_substring(i, chars_read);
        if (len == 0) len = 1;
        size_t len2 = t.find_max_substring(i + len, chars_read);
        len = (len2 == 0) ? len + 1 : len + len2;
        t.insert_substring(i, len);
        parsing.push_back(phrase(i, chars_read));
    }
    return parsing;
}

std::vector<phrase> lzmw_parse(istring_t const &str) {
    substring_trie t(str);
    std::vector<phrase> parsing;
    for (size_t len, last = SIZE_MAX, i = 0; i < str.size(); i += len) {
        size_t chars_read = 0;
        len = t.find_max_substring(i, chars_read);
        if (len == 0) len = 1;
        if (last != SIZE_MAX) t.insert_substring(last, len + i - last);
        last = i;
        parsing.push_back(phrase(i, chars_read));
    }
    return parsing;
}
