/**
* This is a supplementary material for the paper:
*   On Two LZ78-style Grammars: Compression Bounds and Compressed-Space Computation
*   Symposium on String Processing and Information Retrieval (SPIRE), 2017.
* Copyright (C) 2017
*   Tomasz Kociumaka <kociumaka (at) mimuw.edu.pl> (corresponding author),
*   Dmitry Kosolobov <dkosolobov (at) mail.ru> (corresponding author),
*   Golnaz Badkobeh, Travis Gagie, Shunsuke Inenaga, and Simon J. Puglisi.
*
* For the description, read the readme file.
* --------------------------------------------------------------
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
* OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
* HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
* OTHER DEALINGS IN THE SOFTWARE.
**/

#pragma once
#include <vector>
#include <map>
#include <functional>
#include <stdint.h>
#include <string>

typedef std::map<size_t, size_t> ctrl_t;
typedef std::basic_string<uint32_t> istring_t;

struct ctrl_string {
    istring_t str;
    ctrl_t ctrls;
    std::function<std::string(uint32_t)> print;
    
    ctrl_string(): str(), ctrls(), print(nullptr) {}
};

typedef ctrl_string generate_t(uint32_t);

ctrl_string lzd_naive_generate(uint32_t k);
ctrl_string lzmw_naive_generate(uint32_t k);
ctrl_string lzd_large_generate(uint32_t k);
ctrl_string lzmw_large_generate(uint32_t k);
