/**
* This is a supplementary material for the paper:
*   On Two LZ78-style Grammars: Compression Bounds and Compressed-Space Computation
*   Symposium on String Processing and Information Retrieval (SPIRE), 2017.
* Copyright (C) 2017
*   Tomasz Kociumaka <kociumaka (at) mimuw.edu.pl> (corresponding author),
*   Dmitry Kosolobov <dkosolobov (at) mail.ru> (corresponding author),
*   Golnaz Badkobeh, Travis Gagie, Shunsuke Inenaga, and Simon J. Puglisi.
*
* For the description, read the readme file.
* --------------------------------------------------------------
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
* OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
* HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
* OTHER DEALINGS IN THE SOFTWARE.
**/

#include <iostream>
#include <sstream>
#include <vector>
#include <random>
#include <algorithm>
#include <string>
#include <set>
#include <stdint.h>
#include "substring_trie.h"

static bool test_trivial()
{
    istring_t src = istring_t(11, 0);
    substring_trie t(src);
    for (size_t i = 0; i < src.size(); ++i)
        for (size_t j = i; j < src.size(); ++j) {
            t.insert_substring(i, j - i + 1);
            size_t tmp;
            if (t.find_max_substring(src.size() - j + i - 1, tmp) != j - i + 1) return false;
        }
    return true;
}

static bool test_substr(const substring_trie &t, const std::set<istring_t> &set, 
    const istring_t &src, size_t pos, size_t max_len)
{
    size_t exp_len = 0;
    for (size_t len = 1; len <= std::min(src.size() - pos, max_len); ++len)
        if (set.find(src.substr(pos, len)) != set.end()) exp_len = len;
    size_t tmp;
    return exp_len == t.find_max_substring(pos, tmp);
}

static bool test_random()
{
    const size_t src_size = 200, max_len = 20;
    std::random_device rand_dev;
    std::default_random_engine generator(rand_dev());
    std::uniform_int_distribution<uint32_t> rsym('a', 'e');
    std::uniform_int_distribution<size_t> rpos(0, src_size - 1);

    istring_t src(src_size, 0);
    for (size_t i = 0; i < src_size; ++i) src[i] = rsym(generator);
    
    substring_trie t(src);
    std::set<istring_t> set;

    for (size_t cnt = 0; cnt < src_size * 10; ++cnt) {
        size_t i = rpos(generator);
        size_t len = rpos(generator) % std::min(src_size - i, max_len);
        t.insert_substring(i, len);
        set.insert(src.substr(i, len));
        for (int cnt2 = 0; cnt2 < 20; ++cnt2) {
            if (!test_substr(t, set, src, i, max_len)) return false;
            i = rpos(generator);
            len = rpos(generator) % std::min(src_size - i, max_len);
        }
    }
    return true;
}

int main() {
    bool succ = test_trivial();
    for (int cnt = 0; cnt < 10; ++cnt) succ = (succ && test_random());
    std::cerr << "TRIE TEST: " << (succ? "test passed successfully" : "test failed!") << std::endl;
    return !succ;
}

