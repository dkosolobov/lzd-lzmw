/**
* This is a supplementary material for the paper:
*   On Two LZ78-style Grammars: Compression Bounds and Compressed-Space Computation
*   Symposium on String Processing and Information Retrieval (SPIRE), 2017.
* Copyright (C) 2017
*   Tomasz Kociumaka <kociumaka (at) mimuw.edu.pl> (corresponding author),
*   Dmitry Kosolobov <dkosolobov (at) mail.ru> (corresponding author),
*   Golnaz Badkobeh, Travis Gagie, Shunsuke Inenaga, and Simon J. Puglisi.
*
* For the description, read the readme file.
* --------------------------------------------------------------
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
* OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
* HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
* OTHER DEALINGS IN THE SOFTWARE.
**/

#include <vector>
#include <functional>
#include <stdint.h>

#include "generate.h"

static uint32_t k;
#define MIN_SEPARATOR_CHAR_CODE 0x80000000u


static istring_t Rep(int i, int j, std::function<istring_t(int)> f)
{
    if (i > j) return istring_t();
    istring_t res = f(i);
    while (i < j) res += f(++i);
    return res;
}
static istring_t Rep(int i, std::function<istring_t()> f)
{
    if (i <= 0) return istring_t();
    istring_t res = f();
    while (--i > 0) res += f();
    return res;
}
static istring_t S()
{
    static uint32_t separator = MIN_SEPARATOR_CHAR_CODE;
    return istring_t(1, separator++);
}
static istring_t aij(int i, int j) { return istring_t(1, (i - 1)*k + j - 1); }
static istring_t wi(int i) { return Rep(1, k, [i](int j) { return aij(i, j); }); }
static istring_t w() { return Rep(1, k, wi); }
static istring_t spre(int i) { return Rep(2, k, [i](int j) { return wi(i).substr(0, j); }); }
static istring_t ssuf(int i) { return Rep(2, k - 1, [i](int j) { return wi(i).substr(k-j, j); }); }
static istring_t p() { return Rep(1, k, spre) + Rep(1, k, ssuf); }
static istring_t q() { return Rep(1, k - 2, [](int i) { return Rep(k - i - 1, k - 1, wi); }) + w(); }
static istring_t s_()
{
    return p() + q() + Rep(2 * k - 2, w) + Rep(2, k, [](int i) { return wi(k).substr(i-1, k-i+1) + Rep(k, w); });
}
static istring_t uij_(int i, int j) { return wi(k).substr(j-1, k-j+1) + Rep(1, i - 1, wi) + wi(i).substr(0, j); }
static istring_t uij(int i, int j) { return wi(k).substr(j-1, k-j+1) + Rep(1, i - 1, wi) + uij_(i, j); }
static istring_t vij(int i, int j)
{
    return wi(i).substr(j-1, k-j+1) + Rep(i + 1, k - 1, wi) + wi(i).substr(j-1, k-j+1)
        + Rep(i + 1, k - 1, wi) + wi(k).substr(0, j-1);
}
static istring_t xi(int i)
{
    return Rep(2, k - 1, [i](int j) { return (i == 1 ? uij_(i, j):uij(i, j)) + S() + S(); }) + uij_(i, k) + S() + S()
        + Rep(2, k, [i](int j) { return vij(i, j) + S() + S(); });
}
static istring_t zi(int i) { return wi(i).substr(1, k-1) + Rep(i + 1, k, wi) + Rep(k - 2, w) + Rep(1, i, wi); }


ctrl_string lzd_naive_generate(uint32_t _k) {
    k = _k;
    ctrl_string res;
    auto s = s_() + Rep(1, k - 2, [](int i) {return xi(i) + zi(i) + S() + S();});

    //Now we produce "control" positions, described in the paper, on which the naive LZD parser spends a lot of time.
    //This is a cryptic part, it requires the paper text to be understood in details.
    //In general, for every "control" position i, ctrl_positions[i] contains the number of characters that the naive
    //LZD parser should read during the analysis of i (but the phrase found at position i is much shorter).

    size_t jump_len = w().size() + 1;
    size_t pos = s_().size();
    for (int i = 1; i <= k - 2; ++i) {
        pos += xi(i).size(); //move pos to the starting position of z_i
        for (size_t j = 2; j <= k; ++j) {
            //Here pos is a position inside z_i and the string w_i[j..k]w_{i+1}w_{i+2}...w_k occurs at this position.
            //pos is a "control" point; the phrase produced by the LZD parser during the processing of pos constists of 
            //two parts: w_i[j..k]w_{i+1}...w_{k-1}w_k[1..j-1] and w_k[j..k]w_1...w_{i-1}w_i[1..j].
            //The second part is a prefix of w_k[j..k]w^k, which is in the LZD dictionary, and at the same time the
            //second part is a prefix of a longer string w_k[j..k]w^{k-j}w_1...w_i starting at the same position, 
            //which is also a prefix of w_k[j..k]w^k; the length of this longer string is k-j+1+kk(k-j)+ik.
            //The LZD parser reads the whole w_k[j..k]w^{k-j}w_1...w_i during the analysis of this "control" position.
            //Nevertheless, the phrase added to the dictionary after this processing is much shorter; see the paper.
            res.ctrls[pos] = k - j + 1 + k*k*(k - j) + i*k;
            pos += jump_len;
        }
        pos += 2; //skip two separator letters ending z_i; then, pos is at the starting position of x_{i+1}z_{i+1}
    }
    res.str = s;
	res.print = [](uint32_t c) {
        if (c < MIN_SEPARATOR_CHAR_CODE) 
			return  "a{" + std::to_string(c / k + 1) + "," + std::to_string(c % k + 1) + "}";
        else 
			return std::string("#");
    };
    return res;
}
