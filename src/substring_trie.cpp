/**
* This is a supplementary material for the paper:
*   On Two LZ78-style Grammars: Compression Bounds and Compressed-Space Computation
*   Symposium on String Processing and Information Retrieval (SPIRE), 2017.
* Copyright (C) 2017
*   Tomasz Kociumaka <kociumaka (at) mimuw.edu.pl> (corresponding author),
*   Dmitry Kosolobov <dkosolobov (at) mail.ru> (corresponding author),
*   Golnaz Badkobeh, Travis Gagie, Shunsuke Inenaga, and Simon J. Puglisi.
*
* For the description, read the readme file.
* --------------------------------------------------------------
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
* OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
* HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
* OTHER DEALINGS IN THE SOFTWARE.
**/

#include <vector>
#include <map>
#include <stdint.h>

#include "substring_trie.h"

substring_trie::vertex_t substring_trie::new_leaf(vertex_t source_vertex, size_t beg, size_t end)
{
    vertex_t leaf = (vertex_t)maps.size();
    maps.push_back({ });
    refs.push_back(end);
    lens.push_back(end - beg);
    is_marked.push_back(false);
    maps[source_vertex][src[beg]] = leaf;
    return leaf;
}
substring_trie::vertex_t substring_trie::split_edge(vertex_t top_vert, vertex_t bot_vert, size_t split_len)
{
    size_t first_char = refs[bot_vert] - lens[bot_vert];
    vertex_t newv = maps[top_vert][src[first_char]] = (vertex_t)maps.size();
    maps.push_back({ { src[first_char + split_len], bot_vert} });
    refs.push_back(refs[bot_vert] - lens[bot_vert] + split_len);
    lens.push_back(split_len);
    is_marked.push_back(false);
    lens[bot_vert] -= split_len;
    return newv;
}
void substring_trie::insert_substring(size_t i, size_t len)
{
    vertex_t x = 0; //the root of the trie
    size_t j = i;
    while (j < i + len && maps[x].count(src[j])) {
        vertex_t xpar = x;
        x = maps[x].at(src[j]);
        size_t p = refs[x] - lens[x];
        for (; p != refs[x] && j < i + len; ++j, ++p)
            if (src[j] != src[p]) break;
        if (p != refs[x]) x = split_edge(xpar, x, p - (refs[x] - lens[x]));
    }
    if (j < i + len) x = new_leaf(x, j, i + len);
    is_marked[x] = true;
}
size_t substring_trie::find_max_substring(size_t i, size_t &chars_read) const
{
    chars_read = 0;
    size_t res = 0, j = i;
    vertex_t x = 0; //the root of the trie
    while (j < src.size() && maps[x].count(src[j])) {
        x = maps[x].at(src[j]);
        size_t p = refs[x] - lens[x];
        for (; p != refs[x] && j < src.size(); ++j, ++p, ++chars_read)
            if (src[j] != src[p]) return res;
        if (p == refs[x] && is_marked[x]) res = j - i;
    }
    return res;
}
