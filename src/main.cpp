/**
* This is a supplementary material for the paper:
*   On Two LZ78-style Grammars: Compression Bounds and Compressed-Space Computation
*   Symposium on String Processing and Information Retrieval (SPIRE), 2017.
* Copyright (C) 2017
*   Tomasz Kociumaka <kociumaka (at) mimuw.edu.pl> (corresponding author),
*   Dmitry Kosolobov <dkosolobov (at) mail.ru> (corresponding author),
*   Golnaz Badkobeh, Travis Gagie, Shunsuke Inenaga, and Simon J. Puglisi.
*
* For the description, read the readme file.
* --------------------------------------------------------------
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
* OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
* HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
* OTHER DEALINGS IN THE SOFTWARE.
**/

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <cassert>
#include "generate.h"
#include "reduction.h"
#include "parse.h"


static void output_parsing(std::string name, ctrl_string const &s, std::vector<phrase> const &parsing) {
    std::cout << "The " << name << " parsing:" << std::endl;
    auto it = parsing.begin();
    ++it;
    for (size_t i = 0; i < s.str.size(); ++i) {
        if (it != parsing.end() && i == it->beg) {
            ++it;
            std::cout << std::endl;
        }
        std::cout << s.print(s.str[i]);
    }
    std::cout << std::endl;
}

static bool verify(std::string name, ctrl_string const &src, std::vector<phrase> const &parsing) {
    bool okay = true;
    size_t ctrl_read = 0;
    size_t ctrl_visited = 0;
    size_t total_read = 0;
    for  (auto const &p : parsing) {
        total_read += p.read;
        auto ctrl = src.ctrls.find(p.beg);
        if (ctrl != src.ctrls.end()) {
            ++ctrl_visited;
            ctrl_read += p.read;
            if (p.read < ctrl->second) okay = false;
        }
    }
    if (ctrl_visited != src.ctrls.size()) okay = false;
    //Now we check whether the parser does read a lot of additional chars on "control" points described in our paper.
    //See the comments in lzd_gen.cpp for the description of the map ctrl_positions; also see the original paper.
    std::cout << std::setw(35) << "String length: " << std::setw(11) << src.str.size() << std::endl;
    std::cout << std::setw(35) << "Number of phrases: " <<  std::setw(11) << parsing.size() << std::endl;
    std::cout << std::setw(35) << "Characters read on control points: "  << std::setw(11) << ctrl_read << std::endl;
    std::cout << std::setw(35) << "Characters read in total: " << std::setw(11) << total_read << std::endl; 
    std::cout << "The naive " << name << " parser " << (okay ? "worked as expected." : "did not work as expected!!!") << std::endl;
    return okay;
}


static bool test_naive(bool print, uint32_t k, std::string name, 
		generate_t *generate, parse_t *parse, reduction_t *reduction = nullptr) {
    ctrl_string src = generate(k);
    if (reduction) {
        src = reduce(reduction, src);
    }
    auto parsing = parse(src.str);
    if (!verify(name, src, parsing)) return false;
    if (print) output_parsing(name, src, parsing);
    return true;
}

static bool test_large(bool print, uint32_t k, std::string name, 
		generate_t *generate, parse_t *parse, reduction_t *reduction = nullptr) {
    ctrl_string src = generate(k);
    if (reduction) {
        src = reduce(reduction, src);
    }
    auto parsing = parse(src.str);
    std::cout << std::setw(20) << "String length: " << std::setw(11) << src.str.size() << std::endl;
    std::cout << std::setw(20) << "Number of phrases: " <<  std::setw(11) << parsing.size() << std::endl;
    if (print) output_parsing(name, src, parsing);
    return true;
}

int main() {
    std::string parsing;
    std::string option;
    bool print;
    uint32_t k;
    
    
    std::cerr << "Please choose the parsing. [LZMW|LZD]" << std::endl;
    while (true) {
        std::cin >> parsing;
        if (parsing != "LZMW" && parsing != "LZD") {
            std::cerr << "Parsing not recognized!" << std::endl;
		} else break;
    }
    std::cerr << "Please choose the test. [1|2|3]" << std::endl;
    std::cerr << "1. Large parsing compared to the smallest straight-line grammar." << std::endl;
    std::cerr << "2. Worst-case time of the naive parsing algorithm (arbitrary alphabet)." << std::endl;
    std::cerr << "3. Worst-case time of the naive parsing algorithm (binary alphabet)." << std::endl;
    while (true) {
        std::cin >> option;
        if (option != "1" && option != "2" && option != "3") {
            std::cerr << "Test not recognized!" << std::endl;
        } else break;
    }
    std::cerr << "Please specify k." << std::endl;
    while(true) {
        std::cin >> k;
        if (k < 4 || ((k-1)&k) != 0) {
            std::cerr << "The value must must be a power of two that is at least 4" << std::endl;
        } else break;
    }
    std::cerr << "Do you want to print the parsing? [y|n]" << std::endl;
    {
        char answer;
        std::cin >> std::ws >> answer;
        print = (answer == 'y' || answer == 'Y');
    }
    parse_t *parse = (parsing=="LZD")?lzd_parse:lzmw_parse;
    bool okay = false;
    if (option == "1") {
        okay = test_large(print, k, parsing, (parsing=="LZD")?lzd_large_generate:lzmw_large_generate, parse);
    } else {
        reduction_t *reduction=nullptr;
        if (option == "3") {
            reduction = (parsing=="LZD")?lzd_reduction:lzmw_reduction;
        }
        okay = test_naive(print, k, parsing, (parsing=="LZD")?lzd_naive_generate:lzmw_naive_generate, parse, reduction);
    }
    return (okay)?0:1;
}

