/**
* This is a supplementary material for the paper:
*   On Two LZ78-style Grammars: Compression Bounds and Compressed-Space Computation
*   Symposium on String Processing and Information Retrieval (SPIRE), 2017.
* Copyright (C) 2017
*   Tomasz Kociumaka <kociumaka (at) mimuw.edu.pl> (corresponding author),
*   Dmitry Kosolobov <dkosolobov (at) mail.ru> (corresponding author),
*   Golnaz Badkobeh, Travis Gagie, Shunsuke Inenaga, and Simon J. Puglisi.
*
* For the description, read the readme file.
* --------------------------------------------------------------
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
* OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
* HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
* OTHER DEALINGS IN THE SOFTWARE.
**/

#pragma once
#include <map>
#include <vector>
#include <stdint.h>

typedef uint32_t symbol_t;
typedef std::basic_string<symbol_t> istring_t;

class substring_trie
{
    typedef size_t vertex_t;
    std::vector<std::map<symbol_t, vertex_t>> maps;
    std::vector<size_t> lens;
    std::vector<size_t> refs;
    std::vector<bool> is_marked;
    const istring_t &src;
    vertex_t new_leaf(vertex_t source_vertex, size_t beg, size_t end);
    vertex_t split_edge(vertex_t top_vert, vertex_t bot_vert, size_t split_len);
public:
    explicit substring_trie(const istring_t &src)
        : src(src), maps({ {} }), lens({ 0 }), refs({ 0 }), is_marked({ false }) {} //create the root
    void insert_substring(size_t i, size_t len);
    size_t find_max_substring(size_t i, size_t &chars_read) const;
};
void test_trie();


