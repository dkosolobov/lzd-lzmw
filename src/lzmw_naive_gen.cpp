/**
* This is a supplementary material for the paper:
*   On Two LZ78-style Grammars: Compression Bounds and Compressed-Space Computation
*   Symposium on String Processing and Information Retrieval (SPIRE), 2017.
* Copyright (C) 2017
*   Tomasz Kociumaka <kociumaka (at) mimuw.edu.pl> (corresponding author),
*   Dmitry Kosolobov <dkosolobov (at) mail.ru> (corresponding author),
*   Golnaz Badkobeh, Travis Gagie, Shunsuke Inenaga, and Simon J. Puglisi.
*
* For the description, read the readme file.
* --------------------------------------------------------------
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
* OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
* HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
* OTHER DEALINGS IN THE SOFTWARE.
**/

#include <vector>
#include <stdint.h>
#include <cmath>
#include <map>
#include "generate.h"

static uint32_t k;
#define MIN_SEPARATOR_CHAR_CODE 0x80000000u

static istring_t Rep(int i, int j, std::function<istring_t(int)> f)
{
    if (i > j) return istring_t();
    istring_t res = f(i);
    while (i < j) res += f(++i);
    return res;
}
static istring_t Rep(int i, std::function<istring_t()> f)
{
    if (i <= 0) return istring_t();
    istring_t res = f();
    while (--i > 0) res += f();
    return res;
}
static istring_t S()
{
    static uint32_t separator = MIN_SEPARATOR_CHAR_CODE;
    return istring_t(1, separator++);
}
static istring_t aij(int i, int j) { return istring_t(1, (i - 1)*k + j - 1); }

static istring_t wi(int i) { return Rep(1, k, [i](int j) { return aij(i, j); }); }

static istring_t w() { return Rep(1, k, wi); }

static istring_t spre(int i) { return Rep(2, k, [i](int j) { return wi(i).substr(0, j) + S(); }); }

static istring_t ssuf(int i) { return Rep(2, k - 1, [i](int j) { return wi(i).substr(k-j, j) + S(); }); }
static istring_t p() { return Rep(1, k, spre) + Rep(1, k, ssuf); }
static istring_t q() { return Rep(1, k - 2, [](int i) { return Rep(k - i - 1, k - 1, wi) + S(); }) + w() + S(); }
static istring_t s_()
{
    return p() + q() + Rep(1, (int)std::log2(k), [](int pow) { return Rep(1 << pow, w) + S(); })
        + Rep(2, k, [](int i) { return wi(k).substr(i-1, k-i+1) + Rep(k, w) + S(); });
}
static istring_t yi(int j) { return wi(k).substr(j-1, k-j+1) + wi(1) + S() + wi(k).substr(j-1, k-j+1) + wi(1) + wi(2).substr(0, j); }
static istring_t tij(int i, int j) 
{ 
    return wi(i - 2).substr(j, k-j) + wi(i - 1).substr(0, j) + S()
        + wi(i - 1).substr(j, k-j) + wi(i).substr(0, j);
}
static istring_t uij(int i, int j) { return wi(k).substr(j-1, k-j+1) + Rep(1, i - 2, wi) + wi(i - 1).substr(0,j); }
static istring_t vij(int i, int j)
{
    return wi(i).substr(j-1, k-j+1) + Rep(i + 1, k - 1, wi) + S() + wi(i).substr(j-1, k-j+1)
        + Rep(i + 1, k - 1, wi) + wi(k).substr(0, j-1);
}
static istring_t xi(int i)
{
    return Rep(2, k - 1, [i](int j) { return tij(i, j) + S(); })
        +  Rep(2, k, [i](int j) { return uij(i, j) + S(); })
        +  Rep(2, k, [i](int j) { return vij(i, j) + S(); });
}
static istring_t zi(int i) { return wi(i).substr(1, k-1) + Rep(i + 1, k, wi) + Rep(k - 2, w) + Rep(1, i, wi) + S(); }


ctrl_string lzmw_naive_generate(uint32_t _k) {
    k = _k;
    ctrl_string res;
    auto s = s_() + Rep(2, k, yi) + Rep(2, k/2 - 1, [] (int i) { return xi(2*i) + zi(2*i); });

    //Now we produce "control" positions, described in the paper, on which the naive LZMW parser spends a lot of time.
    //This is a cryptic part, it requires the paper text to be understood in details.
    //In general, for every "control" position i, ctrl_positions[i] contains the number of characters that the naive
    //LZMW parser should read during the analysis of i (but the phrase found at position i is much shorter).

    size_t jump_len = w().size() + 1;
    size_t pos = (s_() + Rep(2, k, yi)).size();
    for (int i = 2; i <= (int)k / 2 - 1; ++i) {
        pos += xi(2*i).size(); //move pos to the starting position of z_{2i}
        for (size_t j = 2; j <= k; ++j) { 
            //Here pos is a position inside z_{2i} and the string w_i[j..k]w_{i+1}w_{i+2}...w_k occurs at this position.
            //pos+(k-2i)k is a "control" point at which the string w_k[j..k]w^{k-j}w_1...w_i occurs.
            //This string is a prefix of w_k[j..k]w^k, which is in LZMW dictionary; its length is k-j+1+kk(k-j)+2ik.
            //The LZMW parser reads the whole w_k[j..k]w^{k-j}w_1...w_i during the analysis of this "control" position.
            //Nevertheless, the phrase added to the dictionary after this processing is much shorter; see the paper.
            res.ctrls[pos + (k - 2 * i)*k] = k-j+1 + k*k*(k-j) + 2*i*k;
            pos += jump_len;
        }
        pos++; //skip a separator letter ending z_{2i}; then, pos is at the starting position of x_{2(i+1)}z_{2(i+1)}
    }
    res.str = s;
	res.print = [](uint32_t c) {
        if (c < MIN_SEPARATOR_CHAR_CODE) return  "a{" + std::to_string(c / k + 1) + "," + std::to_string(c % k + 1) + "}";
        else return std::string("#");
    };
    return res;
}


